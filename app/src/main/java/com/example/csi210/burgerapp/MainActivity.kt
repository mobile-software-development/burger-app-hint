package com.example.csi210.burgerapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.web_item.view.*

class MainActivity : AppCompatActivity() {
    var webList = ArrayList<WebsiteItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        webList.add(
            WebsiteItem(
                "Google",
                "https://www.google.com/",
                false
            )
        )
        webList.add(
            WebsiteItem(
                "Facebook",
                "https://www.facebook.com/",
                false
            )
        )
        webList.add(
            WebsiteItem(
                "Amazon",
                "https://www.amazon.com/",
                false
            )
        )
        webList.add(
            WebsiteItem(
                "Microsoft",
                "https://www.microsoft.com/en-us/",
                false
            )
        )

        //Assign the adapter to the layout.
        web_gridView.adapter = GridViewAdapter(webList) {
            //Use intent to launch website.
            val openURL = Intent(Intent.ACTION_VIEW, Uri.parse(it))
            startActivity(openURL)
        }

        show_button.setOnClickListener {
            val checkedWebs = webList.filter { it.checked }
            Toast.makeText(this, checkedWebs.joinToString(",") { it.name }, Toast.LENGTH_LONG)
                .show()
        }
    }

    //Adapter class to generate grid items
    class GridViewAdapter(val websiteList: ArrayList<WebsiteItem>, val click: (String) -> Unit) :
        BaseAdapter() {

        //Return how many items in the grid
        override fun getCount(): Int {
            return websiteList.size
        }

        //Return each item
        override fun getItem(position: Int): Any {
            return websiteList[position]
        }

        //Return the id for each item. Use the position as id
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        //Most important part!!!. Populate the xml file into a visible item in the list.
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val website = this.websiteList[position]
            var webItemView =
                LayoutInflater.from(parent?.context).inflate(R.layout.web_item, parent, false)
            webItemView.web_name.text = website.name
            //Add callback to the TextView. When it is clicked, use the click method to launch the webpage
            webItemView.web_name.setOnClickListener {
                click(website.url)
            }
            //Add callback to the checkbox
            webItemView.web_checkbox.setOnCheckedChangeListener { compoundButton, b ->
                website.checked = b
            }
            return webItemView
        }
    }
}
