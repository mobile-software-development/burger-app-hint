package com.example.csi210.burgerapp

import android.os.Parcelable

class WebsiteItem(
    val name: String,
    val url: String,
    var checked: Boolean
)